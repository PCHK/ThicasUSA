{
    'name' : 'Thicas Down Payment',
    'version' : '11.0.1.0.0',
    'category': 'Invoicing Management',
    'author': 'Portcities Ltd',
    'website': 'http://www.portcities.net/',
    'license': 'AGPL-3',
    'summary': 'Custom Down Payment Report',
    'depends' : ['thicas_product_custom'],
    'data': [
        'views/res_config_settings_views.xml',
        'views/report_down_payment.xml',
        'wizard/sale_down_payment_views.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
