from odoo import _, api, models
from odoo.exceptions import UserError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_down_payment(self):
        # Obtain the Deposit Product
        product_id = self._down_payment_product()

        # Validate the Sale Orders
        sale_order = self.env['sale.order'].browse(self.env.context.get('active_ids', []))

        for order in sale_order:
            down_payment = [x for x in order.mapped('invoice_ids.invoice_line_ids')
                            if x.product_id.id == int(product_id)]

            full_invoice = [x for x in order.mapped('invoice_ids.invoice_line_ids')
                            if x.product_id.id != int(product_id)]

            if order.state in ['draft', 'sent']:
                raise UserError(_('Sale Order %s is not validated.') % (order.name))

            if bool(full_invoice):
                raise UserError(_('Sale Order %s has full invoice created or'
                                  'Down Payment product is changed.') % (order.name))

            if not bool(down_payment):
                raise UserError(
                    _('Sale Order %s does not have any Down Payment.\n'
                      'Please create a Down Payment for this order first.')
                    % (order.name))

            for line in down_payment:
                if line.invoice_id.state == 'draft':
                    raise UserError(_('Down Payment Invoice is not validated.'))

        # Show the Wizard
        return {
            'name': _('Invoice Down Payment'),
            'type': 'ir.actions.act_window',
            'res_model': 'sale.down.payment',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'context': self.env.context,
        }

    def _down_payment_product(self):
        parameters = self.env['ir.config_parameter']
        product_id = parameters.sudo().get_param('sale.default_deposit_product_id')

        if not product_id:
            raise UserError(_('Deposit Product not found!'))

        return product_id

    def _down_payment_invoice(self):
        product_id = self._down_payment_product()
        domain_ids = [x.invoice_id.id for x in self.mapped('invoice_ids.invoice_line_ids')
                      if x.product_id.id == int(product_id)]

        invoices = self.invoice_ids.search([('id', 'in', domain_ids)], order='create_date desc')

        return invoices[0]

    @api.multi
    def down_payment_number(self):
        invoice = self._down_payment_invoice()
        return invoice.number

    @api.multi
    def down_payment_term(self):
        invoice = self._down_payment_invoice()
        return invoice.payment_term_id.name

    @api.multi
    def down_payment_total(self, dp_amount):
        amount = self.amount_untaxed * dp_amount / 100
        return amount

    @api.multi
    def down_payment_lines(self):
        parameters = self.env['ir.config_parameter']
        product_id = parameters.sudo().get_param('sale.default_deposit_product_id')

        lines = []

        for line in self.order_line:
            if line.product_id.id == int(product_id):
                continue
            else:
                lines.append({
                    'name': 'Product name : %s' % line.product_id.name,
                    'quantity': line.product_uom_qty,
                    'uom_id': line.product_uom,
                    'price_unit': line.price_unit,
                    'price_subtotal': line.price_subtotal,
                })

        return lines

    @api.multi
    def _down_payment_report_name(self):
        self.ensure_one()
        return _('%s - Down Payment') % (self.down_payment_number())
