from odoo import api,  models


class ThicasDownPayment(models.AbstractModel):
    _name = 'report.thicas_down_payment.down_payment'

    @api.multi
    def get_report_values(self, docids, data=None):
        docs = self.env['sale.order'].browse(data.get('ids', []))
        return {
            'doc_ids': data.get('ids', []),
            'doc_model': 'sale.order',
            'data': docs,
            'docs': docs,
            'amount_dp': data.get('amount_dp'),
        }
