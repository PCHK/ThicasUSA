from odoo import api, fields, models
from odoo.addons import decimal_precision as dp


class SaleDownPayment(models.TransientModel):
    _name = 'sale.down.payment'

    amount = fields.Float('Down Payment Amount', digits=dp.get_precision('Account'), default=50.0)

    @api.multi
    def create_down_payment(self):
        docids = self.env.context.get('active_ids', [])
        data = {'amount_dp': self.amount, 'ids': docids}
        return self.env.ref('thicas_down_payment.sale_down_payment').report_action(docids, data=data)


class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = 'sale.advance.payment.inv'

    def _prepare_deposit_product(self):
        res = super(SaleAdvancePaymentInv, self)._prepare_deposit_product()

        res['sale_ok'] = False
        res['purchase_ok'] = False

        return res
