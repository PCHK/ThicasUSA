from odoo import fields, models


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    sale_id = fields.Many2one('sale.order', 'Sale Order')

    state = fields.Selection(
        [('draft', 'Draft'), ('open', 'Open'), ('paid', 'Paid'), ('cancel', 'Cancelled')],
        related='invoice_id.state', string='Status', default='draft',
        readonly=True, copy=False, store=True)
