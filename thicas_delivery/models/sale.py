from odoo import fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    charge_ids = fields.One2many('account.invoice.line', 'sale_id', string='Charges', copy=False)
