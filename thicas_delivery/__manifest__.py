{
    'name' : 'Thicas Delivery',
    'version' : '11.0.1.0.0',
    'category': 'Sales',
    'author': 'Portcities Ltd',
    'website': 'http://www.portcities.net/',
    'license': 'AGPL-3',
    'summary': 'Track Delivery',
    'depends' : ['account', 'sale'],
    'data': [
        'views/account_invoice_views.xml',
        'views/sale_views.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
