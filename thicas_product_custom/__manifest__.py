# -*- coding: utf-8 -*-
##############################################################################
#
# This module is developed by Portcities Indonesia
# Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
# All Rights Reserved
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name' : 'Thicas Product Customization',
    'version' : '1.0.0',
    'summary': 'Thicas Product Customization',
    'description': """
        1. Custom Product Brand in Thicas
    """,
    'author' : 'Portcities',
    'category': 'Inventory & Sales',
    'description': """
    - Add brand on product.\n
    Features:\n 
    1. Nu'man Z
        * Create Brand, Product Line, Collection

    2. Elsafitri Damayanti
        * Create Treeview on Brand, Product Line, Collection
        * Create Field on Product Template
    """,        
    'depends' : ['sale', 'product', 'sale_crm','account_reports','base','account'],
    'data': [
        'security/ir.model.access.csv',
        'views/pcs_brand_view.xml',
        'views/inherited_product_template_view.xml',
        'views/res_partner_view.xml',
        'views/crm_lead_view.xml',
        'views/sale_order_view.xml',
        'views/purchase_order_view.xml',
        'views/account_invoice_view.xml',
        # 'views/res_company_view.xml',
        'report/report_templates.xml',
        'report/sale_report_templates.xml',
        'report/account_report.xml',
        'report/report_invoice.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}