# -*- coding: utf-8 -*-
##############################################################################
#
# This module is developed by Portcities Indonesia
# Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
# All Rights Reserved
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _ 

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    status = fields.Selection([
        ('order_confirmed_by_vendor', 'Order confirmed by vendor'),
        ('50_paid', '50% paid'),
        ('100_paid', '100% paid'),
        ('awaiting_customs_clearance', 'Awaiting customs clearance'),
        ('completed_customs_clearance', 'Completed customs clearance'),
        ('delivered_to_site', 'Delivered to site')], "Status", track_visibility='onchange')
    expected_date = fields.Date('Expected Date', track_visibility='onchange')

    @api.multi
    @api.onchange('status')
    def onchange_status(self):
        """
        Update the following fields when the status is changed:
        - Expected Date
        """
        if not self.status:
            self.update({
                'expected_date': False,
            })
            return

        values = {
            'expected_date': False,
        }
        self.update(values)

    @api.multi
    def action_view_so(self):
        """Action button to show Origin SO per PO"""
        saleorders = self.origin.split(', ')
        action = self.env.ref('thicas_product_custom.act_po_2_sale_order').read()[0]
        if len(saleorders) > 1 or len(saleorders) == 1:
            action['domain'] = [('name', 'in', saleorders)]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action
