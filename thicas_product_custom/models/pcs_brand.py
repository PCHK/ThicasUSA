# -*- coding: utf-8 -*-
##############################################################################
#
# This module is developed by Portcities Indonesia
# Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
# All Rights Reserved
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _ 


class ProductBrand(models.Model):
    _name = "product.brand"
    _description = "Brand"

    name = fields.Char(string="Name")
    categ_id = fields.Many2one('product.category', string="Product Category")

    @api.multi
    @api.depends('name', 'categ_id')
    def name_get(self):
        brands = []
        for brand in self:
            name = brand.name
            if brand.categ_id:
                name = '%s / %s' % (brand.categ_id.name, name)
            brands.append((brand.id, name))
        return brands
    
class ProductLine(models.Model):
    _name = "product.line"
    _description = "Name"

    name = fields.Char(string="Name")
    categ_id = fields.Many2one('product.category', string="Product Category")
    brand_id = fields.Many2one('product.brand', string="Brand")

    @api.onchange('categ_id')
    def do_categ(self):
        brand_ids = []
        if self.categ_id:
            category = self.categ_id.id
            brand_src = self.env['product.brand'].search([('categ_id','=',category)])
            for brand in brand_src:
                brand_ids.append(brand.id)
            return {
                    'value': {
                                'brand_id': False,
                                'product_line_id': False,
                                },
                    'domain': {
                                'brand_id': [('id','in',brand_ids)]
                                }
                    }    

class ProductCollection(models.Model):
    _name = "product.collection"
    _description = "Color"

    name = fields.Char(string="Name")
    categ_id = fields.Many2one('product.category', string="Product Category")
    line_id = fields.Many2one('product.line', string="Name")
    brand_id = fields.Many2one('product.brand', string="Brand")
    color_code = fields.Char(string='Code')
    
    @api.onchange('categ_id')
    def onchange_categ(self):
        brand_ids = []
        if self.categ_id:
            brand_src = self.env['product.brand'].search([('categ_id', '=', self.categ_id.id)])
            for brand in brand_src:
                brand_ids.append(brand.id)
            return {
                    'value': {
                                'brand_id': False,
                                'line_id': False,
                                },
                    'domain': {
                                'brand_id': [('id','in',brand_ids)]
                                }
                    }   
            
    @api.onchange('brand_id')
    def onchange_brand(self):
        line_ids = []
        if self.brand_id:
            line_src = self.env['product.line'].search([('brand_id', '=', self.brand_id.id)])
            for line in line_src:
                line_ids.append(line.id)
            return {
                    'value': {
                                'line_id': False,
                                },
                    'domain': {
                                'line_id': [('id','in',line_ids)]
                                }
                    }   

class ProductShape(models.Model):
    _name = "product.shape"
    _description ="Shape"
    _rec_name = "shape"

    shape = fields.Char(string="Shape")


class ProductMaterial(models.Model):
    _name = "product.material"
    _description ="Material"
    _rec_name = "material"

    material = fields.Char(string="Material")
        

class ProductTemplate(models.Model):
    _inherit = "product.template"

    brand_id = fields.Many2one('product.brand', string="Brand")
    product_line_id = fields.Many2one('product.line', string="Name")
    collection_id = fields.Many2one('product.collection', string="Color / Effect")
    width = fields.Char(string="Width")
    depth = fields.Char(string="Depth")
    height = fields.Char(string="Height")
    shape_ids = fields.Many2one('product.shape', string="Shape")
    material_ids = fields.Many2one('product.material', string="Material")
    composition = fields.Text('Composition')
    flammability = fields.Text('Flammability Requirements')
    countrt_origin = fields.Many2one('res.country', string='Country of Origin')
    min_order_qty = fields.Float(string='Minimum Order Quantity', default=0.0, required=True)


    @api.onchange('brand_id')
    def do_brand(self):
        line_ids = []
        if self.brand_id:
            brand = self.brand_id.id
            categ = self.categ_id.id
            line_src = self.env['product.line'].search([('brand_id','=',brand), ('categ_id','=',categ)])
            for line in line_src:
                line_ids.append(line.id)
            return {
                    'value': {
                            'product_line_id': False, 
                            'collection_id': False
                            },
                    'domain': {
                                'product_line_id': [('id','in',line_ids)],
                                }
                    }

    @api.onchange('categ_id')
    def do_categ(self):
        brand_ids = []
        if self.categ_id:
            category = self.categ_id.id
            brand_src = self.env['product.brand'].search([('categ_id','=',category)])
            for brand in brand_src:
                brand_ids.append(brand.id)
            return {
                    'value': {
                                'brand_id': False,
                                'product_line_id': False,
                                'collection_id': False,
                                },
                    'domain': {
                                'brand_id': [('id','in',brand_ids)]
                                }
                    }
            
    @api.onchange('product_line_id')
    def do_line(self):
        line_ids = []
        if self.product_line_id:
            brand = self.brand_id.id
            categ = self.categ_id.id
            line_src = self.env['product.collection'].search([('brand_id','=',brand), ('categ_id','=',categ), ('line_id','=',self.product_line_id.id)])
            for line in line_src:
                line_ids.append(line.id)
            return {
                    'value': {
                            'collection_id': False
                            },
                    'domain': {
                                'collection_id': [('id','in',line_ids)],
                                }
                    }
            
class product_product(models.Model):
    _inherit = "product.product"
    
    brand_id = fields.Many2one('product.brand', string="Brand", related="product_tmpl_id.brand_id")
    product_line_id = fields.Many2one('product.line', string="Name", related="product_tmpl_id.product_line_id")
    collection_id = fields.Many2one('product.collection', string="Color / Effect", related="product_tmpl_id.collection_id")
    width = fields.Char(string="Width", related="product_tmpl_id.width")
    depth = fields.Char(string="Depth", related="product_tmpl_id.depth")
    height = fields.Char(string="Height", related="product_tmpl_id.height")
    shape_ids = fields.Many2one('product.shape', string="Shape", related="product_tmpl_id.shape_ids")
    material_ids = fields.Many2one('product.material', string="Material", related="product_tmpl_id.material_ids")
    composition = fields.Text('Composition', related="product_tmpl_id.composition")
    flammability = fields.Text('Flammability Requirements', related="product_tmpl_id.flammability")
    countrt_origin = fields.Many2one('res.country', string='Country of Origin',
                                     related="product_tmpl_id.countrt_origin")
    min_order_qty = fields.Float(string='Minimum Order Quantity', related="product_tmpl_id.min_order_qty", default=0.0, required=True)

    @api.onchange('brand_id')
    def do_brand(self):
        line_ids = []
        if self.brand_id:
            brand = self.brand_id.id
            categ = self.categ_id.id
            line_src = self.env['product.line'].search([('brand_id','=',brand), ('categ_id','=',categ)])
            for line in line_src:
                line_ids.append(line.id)
            return {
                    'value': {
                            'product_line_id': False, 
                            'collection_id': False
                            },
                    'domain': {
                                'product_line_id': [('id','in',line_ids)],
                                }
                    }

    @api.onchange('categ_id')
    def do_categ(self):
        brand_ids = []
        if self.categ_id:
            category = self.categ_id.id
            brand_src = self.env['product.brand'].search([('categ_id','=',category)])
            for brand in brand_src:
                brand_ids.append(brand.id)
            return {
                    'value': {
                                'brand_id': False,
                                'product_line_id': False,
                                'collection_id': False,
                                },
                    'domain': {
                                'brand_id': [('id','in',brand_ids)]
                                }
                    }
            
    @api.onchange('product_line_id')
    def do_line(self):
        line_ids = []
        if self.product_line_id:
            brand = self.brand_id.id
            categ = self.categ_id.id
            line_src = self.env['product.collection'].search([('brand_id','=',brand), ('categ_id','=',categ), ('line_id','=',self.product_line_id.id)])
            for line in line_src:
                line_ids.append(line.id)
            return {
                    'value': {
                            'collection_id': False
                            },
                    'domain': {
                                'collection_id': [('id','in',line_ids)],
                                }
                    }
