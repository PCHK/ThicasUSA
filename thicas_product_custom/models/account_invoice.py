# -*- coding: utf-8 -*-
##############################################################################
#
# This module is developed by Portcities Indonesia
# Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
# All Rights Reserved
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _
from odoo.tools import float_is_zero, float_compare, pycompat

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    invoicing_contact = fields.Many2one('res.partner', string = 'Invoicing Contact')
    origin_source = fields.Many2one('sale.order', compute='get_origin')
    inv_residual = fields.Monetary(string='Amount Due',
        store=True, help="Remaining amount due.")
    
    def get_origin(self):
        so_origin = self.env['sale.order'].search([('name','=',self.origin)])
        self.origin_source = so_origin.id            
