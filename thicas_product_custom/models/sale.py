# -*- coding: utf-8 -*-
##############################################################################
#
# This module is developed by Portcities Indonesia
# Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
# All Rights Reserved
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _ 
from odoo.exceptions import UserError, AccessError

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    opportunity_customer = fields.Char(string='Opportunity Customer', related='opportunity_id.partner_id.display_name')
    opportunity_contact = fields.Char(string='Opportunity Contact', related='opportunity_id.contact_name')
    order_type = fields.Char(string="Order type")
    other_sales = fields.Many2many("res.users", string="Other sales")

    @api.multi
    def _prepare_invoice(self):
        result = super(SaleOrder, self)._prepare_invoice()
        result.update({'comment': ''})

        return result

class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        result = super(SaleAdvancePaymentInv, self)._create_invoice(order, so_line, amount)
        result.comment = ''

        return result

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    po_number = fields.Many2one('purchase.order',string='PO Number', compute='get_po_number')
    po_status = fields.Selection([
        ('order_confirmed_by_vendor', 'Order confirmed by vendor'),
        ('50_paid', '50% paid'),
        ('100_paid', '100% paid'),
        ('awaiting_customs_clearance', 'Awaiting customs clearance'),
        ('completed_customs_clearance', 'Completed customs clearance'),
        ('delivered_to_site', 'Delivered to site')], "PO Status", compute='get_po_number')
    expected_date = fields.Date(string='Expected Date', compute='get_po_number')

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        res = super(SaleOrderLine, self).product_id_change()
        str_customer = ''
        str_composition = ''
        str_country = ''
        str_description = ''
        str_prodname = ''

        if self.product_id.name:
            str_prodname = 'Product Name :' + self.product_id.name + '\n'        
        if self.product_id.description_sale:
            str_customer = 'Description :' + self.product_id.description_sale + '\n' 
        if self.product_id.composition:
            str_composition = 'Composition : ' + self.product_id.composition + '\n'
        if self.product_id.countrt_origin.name:
            str_country = 'Country of Origin : ' + self.product_id.countrt_origin.name
            
        str_description = str_prodname + str_customer + str_composition + str_country 
        self.name = str_description
        return res

    @api.multi
    def get_po_number(self):
        """ Function to set PO Number,Status,Date on SO Line"""
        is_mto = False
        for line in self: ### each line in SO
            for routes in line.product_id.route_ids: ### set flag is_mto if product routes MTO then is_mto = True
                if routes.id == 1:
                    is_mto = True
            for categ_routes in line.product_id.route_from_categ_ids: ### set flag is_mto if product_categories routes MTO then is_mto = True
                if categ_routes.id == 1:
                    is_mto = True
            if is_mto:
                po = line.env['purchase.order'].search([('origin', 'like', line.order_id.name)])
                for po_data in po: ### PO data from all products in SO line
                    for po_line in po_data.order_line: ### PO lines to look for the products
                        if po_line.product_id.id == line.product_id.id: ### check if product is suitable then update
                            line.po_number = po_data.id
                            line.po_status = po_data.status
                            line.expected_date = po_data.expected_date
